<?php

namespace Drupal\vib_service_authorization;

use Drupal\openid_connect\OpenIDConnect;
use Drupal\openid_connect\OpenIDConnectAuthmap;
use Drupal\user\Entity\User;
use Drupal\vib_service\Client\Model\VibRole;
use Drupal\vib_service\Client\Model\VibTeam;
use Drupal\vib_service\Client\VibServiceClientInterface;

/**
 * Class VibServiceHelper.
 *
 * @package Drupal\vib_service_authorization
 */
class VibServiceHelper {

  protected $authMap;
  protected $openIDConnect;
  protected $client;

  /**
   * VibServiceHelper constructor.
   *
   * @param \Drupal\openid_connect\OpenIDConnectAuthmap $auth_map
   * @param \Drupal\openid_connect\OpenIDConnect $open_id_connect
   * @param \Drupal\vib_service\Client\VibServiceClientInterface $client
   */
  public function __construct(OpenIDConnectAuthmap $auth_map, OpenIDConnect $open_id_connect, VibServiceClientInterface $client) {
    $this->authMap = $auth_map;
    $this->openIDConnect = $open_id_connect;
    $this->client = $client;
  }

  /**
   * @param \Drupal\Core\Session\AccountProxyInterface|\Drupal\Core\Session\AccountInterface|\Drupal\user\UserInterface $account
   * @return array
   */
  public function hasConnectedAccounts($account) {
    return $this->authMap->getConnectedAccounts($account);
  }

  /**
   * @param \Drupal\Core\Session\AccountProxyInterface|\Drupal\Core\Session\AccountInterface|\Drupal\user\UserInterface $account
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function reSyncAccount($account) {
    $connected_accounts = $this->hasConnectedAccounts($account);
    if (!$vib_user_id = reset($connected_accounts)) {
      return FALSE;
    }

    $user_info = [];
    if (!$vib_user = $this->client->apiGetUser($vib_user_id)) {
      return FALSE;
    }
    $vib_user->alterOpenIdUserInfo($user_info);

    $user_info['role'] = array_map(function (VibRole $role) {
      return $role->getMachineName();
    }, $vib_user->getRoles());

    $user_info['vib_teams'] = array_map(function (VibTeam $team) {
      return $team->getMachineName();
    }, $vib_user->getTeams());

    // Store metadata in user data service so other modules can use it.
    /** @var \Drupal\vib_service_authorization\VibServiceUserManager $user_manager */
    $user_manager = \Drupal::service('vib_service_authorization.user_manager');
    $user_manager->saveUserMetaDataForAccount($vib_user->getMetaData(), $account);

    $this->openIDConnect->saveUserinfo(User::load($account->id()), [
      'plugin_id' => 'vib_service_custom_call',
      'userinfo' => $user_info,
    ]);

    return TRUE;
  }

}
