<?php

namespace Drupal\vib_service_authorization;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserDataInterface;
use Drupal\user\UserInterface;
use Drupal\vib_service\Client\Model\VibMetaData;
use Drupal\vib_service\Client\Model\VibRole;
use Drupal\vib_service\Client\Model\VibTeam;

/**
 * Class VibServiceUserManager.
 *
 * @package Drupal\vib_service
 */
class VibServiceUserManager {

  protected $config;
  protected $connection;
  protected $userData;
  protected $account;

  /**
   * VibServiceUserManager constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Database\Connection $connection
   * @param \Drupal\user\UserDataInterface $user_data
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $connection, UserDataInterface $user_data, AccountProxyInterface $account) {
    $this->config = $config_factory->get('vib_service_authorization.settings');
    $this->connection = $connection;
    $this->userData = $user_data;
    $this->account = $account;
  }

  /**
   * Remove the roles from the user, so they can be resynchronized.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   */
  public function removeRoles(UserInterface $user) {
    $roles_never_revoke = array_filter($this->config->get('roles_never_revoke'));
    // First remove all roles. Roles could be revoked in VIB Service.
    foreach ($user->getRoles(TRUE) as $role) {
      // Make sure only roles are removed which are not marked as
      // "NEVER REVOKE" on the config form.
      if (in_array($role, $roles_never_revoke)) {
        continue;
      }

      $user->removeRole($role);
    }
  }

  /**
   * Assign the roles linked to the correct vib roles.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   * @param $context
   *   The context.
   */
  public function assignRoles(UserInterface $user, $context) {
    // Shortcut when no roles defined in service.
    if (empty($context['userinfo']['role'])) {
      $this->saveUserVibRolesForAccount([], $user);
      return;
    }
    if (!is_array($context['userinfo']['role'])) {
      $context['userinfo']['role'] = [$context['userinfo']['role']];
    }

    $vib_roles = $context['userinfo']['role'];
    $this->saveUserVibRolesForAccount($vib_roles, $user);

    foreach ($vib_roles as $vib_role) {
      $vib_role = VibRole::toMachineName($vib_role);
      if (empty($this->config->get('role_mapping')[$vib_role])) {
        continue;
      }

      $mapping = $this->config->get('role_mapping')[$vib_role];
      foreach (array_filter($mapping['drupal_roles']) as $role) {
        if (in_array($role, ['anonymous', 'administrator'])) {
          continue;
        }

        // Assign role.
        $user->addRole($role);
      }
    }
  }

  /**
   * Assign the roles linked to the correct vib teams.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity.
   * @param $context
   *   The context.
   */
  public function assignTeams(UserInterface $user, $context) {
    // Shortcut when no roles defined in service.
    if (empty($context['userinfo']['vib_teams'])) {
      $this->saveUserVibTeamsForAccount([], $user);
      return;
    }

    if (!is_array($context['userinfo']['vib_teams'])) {
      $context['userinfo']['vib_teams'] = [$context['userinfo']['vib_teams']];
    }

    $configured_teams = $this->config->get('team_mapping');
    $vib_teams = $context['userinfo']['vib_teams'];
    foreach ($vib_teams as &$vib_team) {
      $vib_team = VibTeam::toMachineName($vib_team);
      if (!isset($configured_teams[$vib_team])
        || (!$mapping = $configured_teams[$vib_team])) {
        continue;
      }

      foreach (array_filter($mapping['drupal_roles']) as $role) {
        if (in_array($role, ['anonymous', 'administrator'])) {
          continue;
        }

        // Assign role.
        $user->addRole($role);
      }
    }
    $this->saveUserVibTeamsForAccount($vib_teams, $user);
  }

  /**
   * @param \Drupal\user\UserInterface $user
   * @return string
   */
  public function getVibServiceExternalUid(UserInterface $user) {
    $query = $this->connection->select('openid_connect_authmap', 'm');
    $query->fields('m', ['sub']);
    $query->condition('m.uid', $user->id());

    return $query->execute()->fetchField();
  }

  /**
   * @param \Drupal\vib_service\Client\Model\VibMetaData $data
   * @deprecated use saveUserMetaDataForAccount().
   */
  public function saveUserMetaData(VibMetaData $data) {
    $this->saveUserMetaDataForAccount($data, $this->account);
  }

  /**
   * @param \Drupal\vib_service\Client\Model\VibMetaData $data
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function saveUserMetaDataForAccount(VibMetaData $data, AccountInterface $account) {
    $this->userData->set('vib_service', $account->id(), 'user_metadata', $data->toJson());
  }

  /**
   * @param array $roles
   * @deprecated Use saveUserVibRolesForAccount().
   */
  public function saveUserVibRoles(array $roles) {
    $this->saveUserVibRolesForAccount($roles, $this->account);
  }

  /**
   * @param array $roles
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function saveUserVibRolesForAccount(array $roles, AccountInterface $account) {
    if ($account->isAnonymous()) {
      return;
    }
    $this->userData->set('vib_service', $account->id(), 'user_vib_roles', $roles);
  }

  /**
   * @param array $teams
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function saveUserVibTeamsForAccount(array $teams, AccountInterface $account) {
    if ($account->isAnonymous()) {
      return;
    }
    $this->userData->set('vib_service', $account->id(), 'user_vib_teams', $teams);
  }

  /**
   * @return \Drupal\vib_service\Client\Model\VibMetaData
   * @deprecated Use getUserMetaDataForAccount().
   */
  public function getUserMetaData() {
    return $this->getUserMetaDataForAccount($this->account);
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @return \Drupal\vib_service\Client\Model\VibMetaData
   */
  public function getUserMetaDataForAccount(AccountInterface $account) {
    $meta_data = new VibMetaData([]);
    if ($data = $this->userData->get('vib_service', $account->id(), 'user_metadata')) {
      $meta_data->setData((array) $data);
    }
    return $meta_data;
  }

  /**
   * @return array|mixed
   * @deprecated Use getUserVibRolesForAccount().
   */
  public function getUserVibRoles() {
    return $this->getUserVibRolesForAccount($this->account);
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @return array|mixed
   */
  public function getUserVibRolesForAccount(AccountInterface $account) {
    if ($account->isAnonymous()) {
      return [];
    }
    return $this->userData->get('vib_service', $account->id(), 'user_vib_roles') ?: [];
  }

  /**
   * @param \Drupal\Core\Session\AccountInterface $account
   * @return array|mixed
   */
  public function getUserVibTeamsForAccount(AccountInterface $account) {
    if ($account->isAnonymous()) {
      return [];
    }
    return $this->userData->get('vib_service', $account->id(), 'user_vib_teams') ?: [];
  }

}
