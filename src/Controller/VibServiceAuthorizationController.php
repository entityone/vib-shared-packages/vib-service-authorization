<?php

namespace Drupal\vib_service_authorization\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\openid_connect\OpenIDConnectAuthmap;
use Drupal\openid_connect\OpenIDConnectClaims;
use Drupal\openid_connect\OpenIDConnectSession;
use Drupal\openid_connect\Plugin\OpenIDConnectClientManager;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service_authorization\Plugin\OpenIDConnectClient\OpenIDConnectVibServiceClient;
use Drupal\vib_service_authorization\VibServiceUserManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for service authorization.
 */
class VibServiceAuthorizationController extends ControllerBase {

  /**
   * The OpenID connect session.
   *
   * @var \Drupal\openid_connect\OpenIDConnectSession
   */
  protected $session;

  /**
   * The OpenID connect plugin manager.
   *
   * @var \Drupal\openid_connect\Plugin\OpenIDConnectClientManager
   */
  protected $pluginManager;

  /**
   * The OpenID connect claims service.
   *
   * @var \Drupal\openid_connect\OpenIDConnectClaims
   */
  protected $claims;

  /**
   * The VIB service user manager.
   *
   * @var \Drupal\vib_service_authorization\VibServiceUserManager
   */
  protected $userManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The OpenID connect authmap service.
   *
   * @var \Drupal\openid_connect\OpenIDConnectAuthmap
   */
  protected $openIdAuthMap;

  /**
   * VibServiceAuthorizationController constructor.
   *
   * @param \Drupal\openid_connect\OpenIDConnectSession $session
   *   The OpenID connect session.
   * @param \Drupal\openid_connect\Plugin\OpenIDConnectClientManager $plugin_manager
   *   The OpenID connect plugin manager.
   * @param \Drupal\openid_connect\OpenIDConnectClaims $claims
   *   The OpenID connect claims service.
   * @param \Drupal\vib_service_authorization\VibServiceUserManager $user_manager
   *   The VIB service user manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\openid_connect\OpenIDConnectAuthmap $auth_map
   *   The OpenID connect authmap service.
   */
  public function __construct(OpenIDConnectSession $session, OpenIDConnectClientManager $plugin_manager, OpenIDConnectClaims $claims, VibServiceUserManager $user_manager, RequestStack $request_stack, OpenIDConnectAuthmap $auth_map) {
    $this->session = $session;
    $this->pluginManager = $plugin_manager;
    $this->claims = $claims;
    $this->userManager = $user_manager;
    $this->requestStack = $request_stack;
    $this->openIdAuthMap = $auth_map;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openid_connect.session'),
      $container->get('plugin.manager.openid_connect_client'),
      $container->get('openid_connect.claims'),
      $container->get('vib_service_authorization.user_manager'),
      $container->get('request_stack'),
      $container->get('openid_connect.authmap')
    );
  }

  /**
   * Redirect to OpenID login.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
   *   The response object.
   *
   * @throws \Exception
   */
  public function redirectLogin() {
    // Mimic form submit in OpenIDConnectLoginForm.
    try {
      $client = OpenIDConnectVibServiceClient::createFromState();
    }
    catch (VibClientException $e) {
      throw new \Exception($e->getMessage());
    }

    if ($this->currentUser()->isAuthenticated()) {
      // Redirect.
      return new RedirectResponse(Url::fromRoute('<front>')->toString());
    }

    // Store destination to redirect to after auth in SESSION.
    // See "OpenIDConnectSession::saveDestination()"
    if (!$path = $this->requestStack->getCurrentRequest()->query->get('open-id-connect-redirect')) {
      // Default to front page.
      $path = '/';
    }

    $_SESSION['openid_connect_destination'] = [
      $path,
    ];

    $scopes = $this->claims->getScopes();
    $_SESSION['openid_connect_op'] = 'login';

    return $client->authorize($scopes);
  }

  /**
   * Redirect to OpenID logout.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The response object.
   *
   * @throws \Exception
   */
  public function redirectLogout() {
    try {
      $client = OpenIDConnectVibServiceClient::createFromState();
    }
    catch (VibClientException $e) {
      throw new \Exception($e->getMessage());
    }

    // Logout from Drupal.
    user_logout();

    $post_redirect_url = Url::fromRoute('<front>');
    if ($connected_accounts = $this->openIdAuthMap->getConnectedAccounts($this->currentUser())) {
      if (!isset($connected_accounts['vib_service'])) {
        // Normal drupal account. Shortcut with redirect.
        return new RedirectResponse($post_redirect_url->setAbsolute(TRUE)
          ->toString());
      }
    }

    return $client->endSession($post_redirect_url);
  }

  /**
   * Silent authentication.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   */
  public function silentAuth(Request $request) {
    \Drupal::service('page_cache_kill_switch')->trigger();

    if (!empty($request->query->get('error'))) {
      // User is not logged in anymore in VIB Services.
      // logout from Drupal.
      user_logout();

      // @todo show message to user?
    }

    return new Response('');
  }

}
