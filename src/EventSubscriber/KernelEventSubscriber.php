<?php

namespace Drupal\vib_service_authorization\EventSubscriber;

use Drupal\Core\Session\AccountInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Subscribes to kernel events.
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  public function __construct (
    protected AccountInterface $currentUser,
  ) {}

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => 'onRequest',
    ];
  }

  /**
   * Subscribes to the request event.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   *
   * @return void
   */
  public function onRequest(RequestEvent $event) {
    $request = $event->getRequest();
    if ($request->attributes->get('_route') !== 'entity.user.canonical' || !($user = $request->attributes->get('user'))) {
      return;
    }

    $extra_settings = \Drupal::state()->get('vib_service_authorization', []);
    if (!($extra_settings['sync_on_profile'] ?? FALSE)) {
      return;
    }

    // Do we only allow sync when an admin is visiting the profile?
    if (!empty($extra_settings['sync_on_profile_only_admins']) && !$this->currentUser->hasPermission('resync all vib users')) {
      return;
    }

    // Resync user when visiting profile.
    try {
      \Drupal::service('vib_service_authorization.helper')
        ->reSyncAccount($user);
    }
    catch (\Exception | GuzzleException $e) {
      // Log error but otherwise do nothing.
      \Drupal::logger('vib_service_authorization')
        ->error($e->getMessage());
    }
  }

}
