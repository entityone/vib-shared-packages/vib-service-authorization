<?php

namespace Drupal\vib_service_authorization\Routing;

use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;
use Drupal\vib_service_authorization\Plugin\OpenIDConnectClient\OpenIDConnectVibServiceClient;
use Drupal\vib_service_authorization\VibServiceHelper;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * @package Drupal\vib_service_authorization\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  protected $vibServiceHelper;
  protected $account;
  protected $openIdClient;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\vib_service_authorization\VibServiceHelper $helper
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   */
  public function __construct(VibServiceHelper $helper, AccountProxyInterface $account) {
    $this->vibServiceHelper = $helper;
    $this->account = $account;
    try {
      $this->openIdClient = OpenIDConnectVibServiceClient::createFromState();
    }
    catch (\Exception $e) {
      // Can occur when module isn't configured yet.
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('user.login')) {
      // Move Drupal user login form to new path.
      $route->setPath('/drupal/login');
    }

    if ($route = $collection->get('user.logout')) {
      // Alter the user logout path and make sure it is not accessible.
      // All logging out goes via "vib_service_authorization.logout" route.
      $route->setPath('/drupal/logout');
      $route->setRequirement('_access', 'FALSE');
    }

    if ($route = $collection->get('user.register')) {
      // Move Drupal user register form to new path.
      $route->setPath('/drupal/register');
    }

    if ($route = $collection->get('entity.user.edit_form')) {
      // Add a new route for the user edit form which always displays the drupal user edit form and never redirects to VIB Services.
      $new_route = clone $route;
      $new_route->setPath('user/{user}/edit-drupal');
      $collection->add('entity.user.edit_form_drupal', $new_route);
    }
  }

  /**
   * Redirect user to VIB Services if account is connected.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function onKernelRequestUserEditPage(RequestEvent $event) {
    $request = $event->getRequest();
    $route_match = RouteMatch::createFromRequest($request);

    if ($request->query->get('sync-user-profile')) {
      // Check if we can fetch the user from the route.
      // If so, we will use this account to update the info.
      if (!$account = $route_match->getParameter('user')) {
        // If not, use the logged in account.
        $account = $this->account;
      }

      if (!$account instanceof AccountInterface) {
        if (!$account = User::load($account)) {
          return;
        }
      }

      if ($this->account->isAuthenticated() && $this->vibServiceHelper->hasConnectedAccounts($account)) {
        // User was redirected back from profile update in VIB Services.
        // We have to re-sync the user data to make sure everything is up-to-date on Drupal user object.
        $this->vibServiceHelper->reSyncAccount($account);
      }
    }

    if ($route_match->getRouteName() === 'entity.user.edit_form' && $this->account->isAuthenticated()) {
      /** @var \Drupal\user\UserInterface $user */
      if (!$user = $route_match->getParameter('user')) {
        return;
      }

      if ($user->id() == $this->account->id()) {
        if ($this->vibServiceHelper->hasConnectedAccounts($this->account)) {
          // User is editing his/her own profile. Redirect to VIB Services profile if there is a connected account.
          $redirect = Url::fromRoute('entity.user.canonical', ['user' => $this->account->id()], [
            'absolute' => TRUE,
            'query' => ['sync-user-profile' => TRUE],
          ]);

          // Unset destination query.
          $request->query->remove('destination');

          $event->setResponse(
            new TrustedRedirectResponse($this->openIdClient->getConfiguration()['base_uri'] . '/profile/#/edit?returnUrl=' . $redirect->toString())
          );
        }

        // Shortcut and just show the drupal user edit form.
        return;
      }

      if (($connected_accounts = $this->vibServiceHelper->hasConnectedAccounts($user)) && isset($connected_accounts['vib_service'])) {
        // User is editing someone else's profile. Redirect to that user's VIB Services profile.
        $redirect = Url::fromRoute('entity.user.canonical', ['user' => $user->id()], [
          'absolute' => TRUE,
          'query' => ['sync-user-profile' => TRUE],
        ]);

        // Unset destination query.
        $request->query->remove('destination');

        $event->setResponse(
          new TrustedRedirectResponse($this->openIdClient->getConfiguration()['base_uri'] . '/admin/#/user/' . $connected_accounts['vib_service'] . '/edit?returnUrl=' . $redirect->toString())
        );

        return;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = parent::getSubscribedEvents();
    $events[KernelEvents::REQUEST][] = ['onKernelRequestUserEditPage'];

    return $events;
  }

}
