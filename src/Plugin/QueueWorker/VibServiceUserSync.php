<?php

namespace Drupal\vib_service_authorization\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\user\UserInterface;
use Drupal\vib_service_authorization\VibServiceHelper;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * QueueWorker that syncs user information from vib services to Drupal.
 *
 * @QueueWorker(
 *   id = "vib_service_user_sync",
 *   title = @Translation("VIB Service User Information Sync"),
 *   cron = {"time" = 15}
 * )
 */
class VibServiceUserSync extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The vib service helper.
   *
   * @var \Drupal\vib_service_authorization\VibServiceHelper
   */
  protected $vibServiceHelper;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a queue worker.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\vib_service_authorization\VibServiceHelper $vib_service_helper
   *   The vib service helper.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    VibServiceHelper $vib_service_helper,
    LoggerChannelFactoryInterface $logger_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->vibServiceHelper = $vib_service_helper;
    $this->logger = $logger_factory->get('vib_service_authorization');
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('vib_service_authorization.helper'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function processItem($data) {
    /**
     * @var \Drupal\user\UserInterface $user
     */
    $user = $this->entityTypeManager
      ->getStorage('user')
      ->load($data['uid']);

    if (!$user instanceof UserInterface) {
      throw new \BadMethodCallException('The user with uid ' . $data['uid'] . ' does not exist.');
    }

    try {
      $this->vibServiceHelper
        ->reSyncAccount($user);
    }
    catch (\Exception | GuzzleException $e) {
      // Log error but otherwise do nothing.
      $this->logger->error($e->getMessage());
    }
  }

}
