<?php

namespace Drupal\vib_service_authorization\Plugin\OpenIDConnectClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Render\Element;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\openid_connect\Plugin\OpenIDConnectClient\OpenIDConnectGenericClient;
use Drupal\user\UserDataInterface;
use Drupal\vib_service\Client\VibClientException;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @OpenIDConnectClient(
 *   id = "vib_service",
 *   label = @Translation("VIB Service")
 * )
 */
class OpenIDConnectVibServiceClient extends OpenIDConnectGenericClient implements OpenIDConnectVibServiceClientInterface {

  use MessengerTrait;

  const GENERIC_AUTH_DOMAIN_ENDPOINT_AUTHORIZE = 'https://auth.services.vib.be/connect/authorize';
  const GENERIC_AUTH_DOMAIN_ENDPOINT_END_SESSION = 'https://auth.services.vib.be/connect/endsession';
  const GENERIC_AUTH_DOMAIN_CALLBACK_LOGIN = 'https://auth.services.vib.be/signin-oidc';
  const GENERIC_AUTH_DOMAIN_CALLBACK_LOGOUT = 'https://auth.services.vib.be/signout-oidc';

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The user data service.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * OpenIDConnectVibServiceClient constructor.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\user\UserDataInterface $user_data
   *   The user data service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, UserDataInterface $user_data) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $request_stack, $http_client, $logger_factory);
    $this->userData = $user_data;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('http_client'),
      $container->get('logger.factory'),
      $container->get('user.data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'base_uri' => 'https://services.vib.be',
      'authorization_endpoint' => 'https://services.vib.be/connect/authorize',
      'end_session_endpoint' => 'https://services.vib.be/connect/endsession',
      'token_endpoint' => 'https://services.vib.be/connect/token',
      'userinfo_endpoint' => 'https://services.vib.be/connect/userinfo',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $weight = 0;
    // Add a weight to each parent form element so we can easily slip our own in between.
    foreach (Element::children($form) as $child) {
      $form[$child]['#weight'] = $weight;
      $weight += 5;
    }

    $form['base_uri'] = [
      '#title' => $this->t('Base URI'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['base_uri'],
      '#description' => $this->t('Base URL of the VIB Services platform'),
      '#weight' => 5,
    ];

    $form['end_session_endpoint'] = [
      '#title' => $this->t('End session endpoint'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['end_session_endpoint'],
      '#description' => $this->t('Endpoint to end the session of the current logged in user'),
      '#weight' => 16,
    ];

    $form['generic_auth_domain_info'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Generic authentication domain'),
      '#open' => FALSE,
      '#weight' => 17,
      [
        '#markup' => '<a href="https://auth.services.vib.be" target="_blank">https://auth.services.vib.be</a>
is the generic authentication domain which can be used for example to route different sites to the same VIB Services app.
If you want to use the generic authentication domain, configure following URIs :
<ul>
<li><strong>Authorization endpoint:</strong> ' . self::GENERIC_AUTH_DOMAIN_ENDPOINT_AUTHORIZE . '</li>
<li><strong>End session endpoint:</strong> ' . self::GENERIC_AUTH_DOMAIN_ENDPOINT_END_SESSION . '</li>
<li><strong>Post Login Redirect URL in VIB Services:</strong> ' . self::GENERIC_AUTH_DOMAIN_CALLBACK_LOGIN . '</li>
<li><strong>Post Logout Redirect URL in VIB Services:</strong> ' . self::GENERIC_AUTH_DOMAIN_CALLBACK_LOGOUT . '</li>
</ul>',
      ],
    ];

    $form['openid_config_url'] = [
      '#type' => 'item',
      '#title' => $this->t('OpenID Configuration info'),
      '#markup' => 'https://services.vib.be/.well-known/openid-configuration',
      '#weight' => 50,
    ];

    $config = \Drupal::config('vib_service.settings');
    $form['client_id']['#default_value'] = $this->configuration['client_id'] ?? $config->get('client_id');
    $form['client_secret']['#default_value'] = $this->configuration['client_secret'] ?? $config->get('client_secret');
    $form['token_endpoint']['#default_value'] = $this->configuration['token_endpoint'] ?? $config->get('token_endpoint');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);
    // Get plugin setting values.
    $configuration = $form_state->getValues();

    if (trim($configuration['authorization_endpoint']) === self::GENERIC_AUTH_DOMAIN_ENDPOINT_AUTHORIZE
      && trim($configuration['end_session_endpoint']) !== self::GENERIC_AUTH_DOMAIN_ENDPOINT_END_SESSION) {
      $form_state->setErrorByName('end_session_endpoint', $this->t('The "authorization" endpoint uses the generic authentication domain, but the "end session" endpoint does not...'));
    }
    if (trim($configuration['authorization_endpoint']) !== self::GENERIC_AUTH_DOMAIN_ENDPOINT_AUTHORIZE
      && trim($configuration['end_session_endpoint']) === self::GENERIC_AUTH_DOMAIN_ENDPOINT_END_SESSION) {
      $form_state->setErrorByName('authorization_endpoint', $this->t('The "end session" endpoint uses the generic authentication domain, but the "authorization" endpoint does not...'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function authorize($scope = 'openid email') {
    // Override scopes.
    $scope = 'openid email userroles';
    return parent::authorize($scope);
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlOptions($scope, GeneratedUrl $redirect_uri) {
    $options = parent::getUrlOptions($scope, $redirect_uri);
    if ($this->usesGenericAuthDomain()) {
      // Set VIB Services environment as query param.
      $options['query']['env'] = $this->getVibServicesEnvironment();
      // In this case the "getRedirectUrl()" method
      // Returns the generic auth domain.
      // But for authorization we need to be able to redirect back to the original domain.
      $options['query']['redirect_uri'] = Url::fromRoute('openid_connect.redirect_controller_redirect',
        [
          'client_name' => $this->pluginId,
        ],
        [
          'language' => $this->languageManager
            ->getLanguage(LanguageInterface::LANGCODE_NOT_APPLICABLE),
        ])
        ->setAbsolute(TRUE)
        ->toString(TRUE)
        ->getGeneratedUrl();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl(array $route_parameters = [], array $options = []) {
    if (!$this->usesGenericAuthDomain()) {
      return parent::getRedirectUrl($route_parameters, $options);
    }

    // Wil be used in the "retrieveTokens()" method.
    return Url::fromUri(self::GENERIC_AUTH_DOMAIN_CALLBACK_LOGIN)
      ->setAbsolute(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveTokens($authorization_code) {
    $tokens = parent::retrieveTokens($authorization_code);
    // Store the tokens in user data.
    $current_user = \Drupal::currentUser();
    $this->userData->set('vib_service', $current_user->id(), 'tokens', $tokens);
    return $tokens;
  }

  /**
   * {@inheritdoc}
   */
  public function endSession(Url $post_redirect_url) {
    $current_user = \Drupal::currentUser();
    $tokens = $this->userData->get('vib_service', $current_user->id(), 'tokens');

    $url_options = [
      'query' => [
        'post_logout_redirect_uri' => $post_redirect_url->setAbsolute(TRUE)
          ->toString(TRUE)->getGeneratedUrl(),
        'id_token_hint' => isset($tokens['id_token']) ? $tokens['id_token'] : NULL,
      ],
    ];

    if ($this->usesGenericAuthDomain()) {
      // Set VIB Services environment as query param.
      $url_options['query']['env'] = $this->getVibServicesEnvironment();
    }

    $endpoints = $this->getEndpoints();
    $end_session_endpoint = Url::fromUri($endpoints['end_session'], $url_options)
      ->toString(TRUE);
    $response = new TrustedRedirectResponse($end_session_endpoint->getGeneratedUrl());
    $response->addCacheableDependency($end_session_endpoint);
    // We can't cache the response, since this will prevent the state to be
    // added to the session. The kill switch will prevent the page getting
    // cached for anonymous users when page cache is active.
    \Drupal::service('page_cache_kill_switch')->trigger();

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints() {
    return [
      'end_session' => $this->configuration['end_session_endpoint'],
    ] + parent::getEndpoints();
  }

  /**
   * Returns a flag indicating if generic auth domain is used for authorization.
   *
   * @return bool
   *   Flag indicating if the generic auth domain is used for authorization.
   */
  protected function usesGenericAuthDomain() {
    return $this->configuration['authorization_endpoint'] === self::GENERIC_AUTH_DOMAIN_ENDPOINT_AUTHORIZE
      && $this->configuration['end_session_endpoint'] === self::GENERIC_AUTH_DOMAIN_ENDPOINT_END_SESSION;
  }

  /**
   * Returns the VIB Services environment to use.
   *
   * @return string
   *   The environment.
   */
  protected function getVibServicesEnvironment() {
    return $this->configuration['base_uri'] === 'https://services.vib.be' ? 'live' : 'staging';
  }

  /**
   * {@inheritdoc}
   */
  public static function createFromState() {
    if (!\Drupal::config('openid_connect.settings.vib_service')
      ->get('enabled')) {
      throw new VibClientException('VIB Service OpenID Connect plugin was not enabled.');
    }

    $manager = \Drupal::service('plugin.manager.openid_connect_client');
    $configuration = \Drupal::config('openid_connect.settings.vib_service')
      ->get('settings');
    return $manager->createInstance('vib_service', $configuration);
  }

}
