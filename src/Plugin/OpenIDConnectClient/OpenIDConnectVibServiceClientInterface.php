<?php

namespace Drupal\vib_service_authorization\Plugin\OpenIDConnectClient;

use Drupal\Core\Url;
use Drupal\openid_connect\Plugin\OpenIDConnectClientInterface;

/**
 * Interface OpenIDConnectVibServiceClientInterface.
 *
 * @package Drupal\vib_service\Plugin\OpenIDConnectClient
 */
interface OpenIDConnectVibServiceClientInterface extends OpenIDConnectClientInterface {

  /**
   * @return \Drupal\vib_service_authorization\Plugin\OpenIDConnectClient\OpenIDConnectVibServiceClientInterface
   * @throws \Drupal\vib_service\Client\VibClientException
   */
  public static function createFromState();

  /**
   * @param \Drupal\Core\Url $post_redirect_url
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function endSession(Url $post_redirect_url);

}
