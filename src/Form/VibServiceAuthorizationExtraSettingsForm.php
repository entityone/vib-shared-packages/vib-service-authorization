<?php

namespace Drupal\vib_service_authorization\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the extra settings form for the vib service authorization module.
 */
class VibServiceAuthorizationExtraSettingsForm extends FormBase {

  /**
   * Constructs the form class.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    protected StateInterface $state
  ) {}

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('state'));
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'vib_service_authorization_extra_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $default_values = $this->state->get('vib_service_authorization', []);

    $form['sync_on_profile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Initiate sync with VIB Services when visiting user profile'),
      '#default_value' => $default_values['sync_on_profile'] ?? FALSE,
    ];

    $form['sync_on_profile_only_admins'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Only initiate this sync when an admin visits this user profile'),
      '#description' => $this->t('When leaving this setting unchecked, all user profile visits will trigger a resync of the user object.'),
      '#default_value' => $default_values['sync_on_profile_only_admins'] ?? FALSE,
      '#states' => [
        'visible' => [
          ':input[name="sync_on_profile"]' => ['checked' => TRUE]
        ],
      ],
    ];

    $form['actions'] = [
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $sync_on_profile = $form_state->getValue('sync_on_profile') ?? FALSE;
    $this->state->set('vib_service_authorization', [
      'sync_on_profile' => $sync_on_profile,
      'sync_on_profile_only_admins' => ($sync_on_profile) ? ($form_state->getValue('sync_on_profile_only_admins') ?? FALSE) : FALSE,
    ]);
  }

}
