<?php

namespace Drupal\vib_service_authorization\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service\Client\VibServiceClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VibServiceAuthorizationSettingsForm.
 *
 * @package Drupal\vib_service_authorization\Form
 */
class VibServiceAuthorizationSettingsForm extends ConfigFormBase {

  /**
   * The VIB client.
   *
   * @var \Drupal\vib_service\Client\VibServiceClientInterface
   */
  protected $vibClient;

  /**
   * VibServiceAuthorizationSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\vib_service\Client\VibServiceClientInterface $vib_client
   *   The VIB client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, VibServiceClientInterface $vib_client) {
    parent::__construct($config_factory);
    $this->vibClient = $vib_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('vib_service.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vib_service_authorization.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vib_service_authorization_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('vib_service_authorization.settings');

    $roles = array_map([
      '\Drupal\Component\Utility\Html',
      'escape',
    ], user_role_names(TRUE));
    unset($roles['authenticated']);

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Drupal login roles'),
      '#default_value' => $settings->get('roles'),
      '#required' => TRUE,
      '#description' => $this->t('Roles who still can login using their Drupal account. All other roles should authenticate through the VIB service.'),
      '#options' => $roles,
      '#disabled' => !$this->currentUser()->hasPermission('administer vib service'),
    ];

    $form['roles_never_revoke'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Never revoke these roles'),
      '#default_value' => $settings->get('roles_never_revoke'),
      '#required' => TRUE,
      '#description' => $this->t('Roles which should never be revoked when syncing roles with VIB Service.'),
      '#options' => $roles,
      '#disabled' => !$this->currentUser()->hasPermission('administer vib service'),
    ];

    // Do not allow to map to admin role.
    unset($roles['administrator']);

    // Make the vib service team to drupal role mapping.
    try {
      // Use API call to determine roles.
      $vib_service_teams = $this->vibClient->apiGetTeams();

      $form['team_mapping_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('VIB Service teams VS Drupal role mapping'),
        '#open' => FALSE,
      ];

      $team_mapping = $settings->get('team_mapping');
      $form['team_mapping_wrapper']['team_mapping'] = [
        '#prefix' => '<p></p>',
        '#type' => 'table',
        '#header' => [
          ['data' => $this->t('VIB Service team')],
          ['data' => $this->t('Drupal roles')],
        ],
      ];

      foreach ($vib_service_teams as $vib_service_team) {
        $form['team_mapping_wrapper']['team_mapping'][$vib_service_team->getFieldKey()]['vib_service_team'] = [
          '#markup' => $vib_service_team->getName(),
        ];

        $form['team_mapping_wrapper']['team_mapping'][$vib_service_team->getFieldKey()]['drupal_roles'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Role mapping'),
          '#title_display' => 'invisible',
          '#options' => $roles,
          '#default_value' => $team_mapping[$vib_service_team->getFieldKey()]['drupal_roles'] ?? [],
          '#description' => $this->t('Assign the team(s) that need to be assigned when a user logs in with the VIB service ":team" team', [':team' => $vib_service_team->getName()]),
        ];
      }

    }
    catch (VibClientException $e) {
      $this->messenger()
        ->addError($this->t('Could not connect to the VIB Service API. Please provide the correct <a href="@href">settings</a>.', [
          '@href' => Url::fromRoute('vib_service.settings')
            ->toString(),
        ]));
    }

    // Make the vib service role to drupal role mapping (DEPRECATED).
    try {
      // Use API call to determine roles.
      $vib_service_roles = $this->vibClient->apiGetRoles();

      $form['role_mapping_wrapper'] = [
        '#type' => 'details',
        '#title' => $this->t('VIB Service VS Drupal role mapping (deprecated)'),
        '#open' => FALSE,
      ];

      $role_mapping = $settings->get('role_mapping');
      $form['role_mapping_wrapper']['role_mapping'] = [
        '#prefix' => '<p></p>',
        '#type' => 'table',
        '#header' => [
          ['data' => $this->t('VIB Service role')],
          ['data' => $this->t('Drupal roles')],
        ],
      ];

      foreach ($vib_service_roles as $num => $vib_service_role) {
        $form['role_mapping_wrapper']['role_mapping'][$vib_service_role->getMachineName()]['vib_service_role'] = [
          '#markup' => $vib_service_role->getName(),
        ];

        $form['role_mapping_wrapper']['role_mapping'][$vib_service_role->getMachineName()]['drupal_roles'] = [
          '#type' => 'checkboxes',
          '#title' => $this->t('Role mapping'),
          '#title_display' => 'invisible',
          '#options' => $roles,
          '#default_value' => $role_mapping[$vib_service_role->getMachineName()]['drupal_roles'] ?? [],
          '#description' => $this->t('Assign the role(s) that needs to be assigned when a user logs in with the VIB service ":role" role', [':role' => $vib_service_role->getName()]),
        ];
      }

    }
    catch (VibClientException $e) {
      $this->messenger()
        ->addError($this->t('Could not connect to the VIB Service API. Please provide the correct <a href="@href">settings</a>.', [
          '@href' => Url::fromRoute('vib_service.settings')
            ->toString(),
        ]));
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('vib_service_authorization.settings');
    $settings->set('roles', $form_state->getValue('roles'))
      ->set('roles_never_revoke', $form_state->getValue('roles_never_revoke'))
      ->set('team_mapping', $form_state->getValue('team_mapping'))
      ->set('role_mapping', $form_state->getValue('role_mapping'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
