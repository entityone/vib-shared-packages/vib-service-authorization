<?php

namespace Drupal\vib_service_authorization\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\user\UserInterface;
use Drupal\vib_service_authorization\VibServiceHelper;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Resynchronizes all users from VIB services.
 */
class VibServiceReSyncUsersForm extends FormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The vib service helper.
   *
   * @var \Drupal\vib_service_authorization\VibServiceHelper
   */
  protected $vibServiceHelper;

  /**
   * VibServiceAuthorizationSettingsForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\vib_service_authorization\VibServiceHelper $vib_service_helper
   *   The VIB service helper.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, VibServiceHelper $vib_service_helper, LoggerChannelFactoryInterface $logger_factory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->vibServiceHelper = $vib_service_helper;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('vib_service_authorization.helper'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'vib_service_resync_users_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Are you sure you wish to resync every user from VIB services?'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => "I'm sure!",
    ];

    $form['actions']['back'] = [
      '#type' => 'button',
      '#value' => $this->t('Back'),
      '#attributes' => ['onclick' => 'window.history.back();'],
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uids = $this->entityTypeManager
      ->getStorage('user')
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();

    $batch = [
      'init_message' => $this->t('Starting sync of @count users', [
        '@count' => count($uids),
      ]),
      'operations' => [
        [
          [$this, 'processBatch'],
          [array_values($uids)],
        ],
      ],
    ];
    batch_set($batch);
  }

  /**
   * Process the user sync batch.
   *
   * @param $data
   *   The uids.
   * @param $context
   *   The context.
   */
  public function processBatch($data, &$context) {
    // Initiate multistep processing.
    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($data);
    }

    // Process the next 25 if there are at least 25 left. Otherwise,
    // we process the remaining number.
    $batch_size = 25;
    $max = $context['sandbox']['progress'] + $batch_size;
    if ($max > $context['sandbox']['max']) {
      $max = $context['sandbox']['max'];
    }

    // Start where we left off last time.
    $start = $context['sandbox']['progress'];
    for ($i = $start; $i < $max; $i++) {
      $uid = $data[$i];
      $user = $this->entityTypeManager
        ->getStorage('user')
        ->load($uid);

      if ($user instanceof UserInterface) {
        $context['message'] = $this->t('Processing user @user (@current/@max)', [
          '@user' => $user->getDisplayName(),
          '@current' => $context['sandbox']['progress'],
          '@max' => $context['sandbox']['max'],
        ]);
        try {
          $this->vibServiceHelper
            ->reSyncAccount($user);
        }
        catch (\Exception | GuzzleException $e) {
          // Log error but otherwise do nothing, otherwise the batch will stop.
          $this->logger('vib_service_authorization')
            ->error($e->getMessage());
        }
      }
      else {
        $context['message'] = $this->t('User with id @uid does not exist.', [
          '@uid' => $uid,
        ]);
      }

      // Update our progress!
      $context['sandbox']['progress']++;
    }

    // Multistep processing : report progress.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

}
