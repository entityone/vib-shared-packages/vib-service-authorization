<?php

namespace Drupal\vib_service_authorization\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\vib_service_authorization\VibServiceHelper;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Resynchronizes individual users from VIB services.
 */
class VibServiceReSyncUserForm extends FormBase {

  /**
   * The vib service helper.
   *
   * @var \Drupal\vib_service_authorization\VibServiceHelper
   */
  protected $vibServiceHelper;

  /**
   * VibServiceAuthorizationSettingsForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\vib_service_authorization\VibServiceHelper $vib_service_helper
   *   The VIB service helper.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   */
  public function __construct(
    VibServiceHelper $vib_service_helper,
    LoggerChannelFactoryInterface $logger_factory,
    RequestStack $request_stack,
  ) {
    $this->vibServiceHelper = $vib_service_helper;
    $this->loggerFactory = $logger_factory;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vib_service_authorization.helper'),
      $container->get('logger.factory'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'vib_service_resync_user_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Are you sure you wish to resync this user from VIB services?'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => "I'm sure!",
    ];

    $form['actions']['back'] = [
      '#children' => '<a href="javascript:void(0);" onclick="window.history.back();" class="button">' . $this->t('Back') . '</a>',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\user\UserInterface $user */
    if ($user = $this->getRequest()->attributes->get('user')) {
      try {
        if (!$this->vibServiceHelper->reSyncAccount($user)) {
          throw new \Exception("Either we couldn't connect to VIB Services, or this user has no connected accounts.");
        }

        $this->messenger()->addMessage('Synchronization of user ' . $user->getEmail() . ' complete.');
        return $this->redirect('entity.user.collection');
      }
      catch (\Exception | GuzzleException $e) {
        // Log error but otherwise do nothing.
        $this->logger('vib_service_authorization')
          ->error($e->getMessage());
      }
    }
    else {
      $this->logger('vib_service_authorization')
        ->error('User was not loaded.');
    }

    $this->messenger()->addError('Synchronization of user ' . $user->getEmail() . ' failed, there should be more information in the logs.');
    return $this->redirect('entity.user.collection');
  }

}
