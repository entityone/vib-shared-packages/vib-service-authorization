<?php

namespace Drupal\vib_service_authorization;

use Drupal\Core\Url;
use Drupal\user\ToolbarLinkBuilder;

/**
 * VibServiceToolbarLinkBuilder will fill the placeholders in user_toolbar().
 */
class VibServiceToolbarLinkBuilder extends ToolbarLinkBuilder {

  /**
   * {@inheritdoc}
   */
  public function renderToolbarLinks() {
    $build = parent::renderToolbarLinks();
    $build['#links']['logout']['url'] = Url::fromRoute('vib_service_authorization.logout');
    return $build;
  }

}
