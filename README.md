This module integrates Drupal with the VIB Service.
It allows Drupal user to authorize through the VIB service.

# Installation

* Add the following GIT repository to your composer.json repositories:

```json
{
    "type": "git",
    "url": "https://gitlab.com/entityone/vib-shared-packages/vib-service-authorization.git"
}
```

* Download module and dependent packages by running
  `composer require drupal/vib_service_authorization:version`
  For example `composer require drupal/vib_service_authorization:^1.0-alpha1`

  Available versions: https://gitlab.com/entityone/vib-shared-packages/vib-service-authorization/-/tags
* Install module by running `drush en vib_service_authorization`

# Configuration

* Navigate to `/admin/config/services/openid-connect`, choose "VIB Service"
  client and fill out the form.
* For the endpoints use:
  * API endpoint: `https://services.vib.be/api/v1`
  * Authorization endpoint: `https://services.vib.be/connect/authorize`
  * End session endpoint: `https://services.vib.be/connect/endsession`
  * Token endpoint: `https://services.vib.be/connect/token`
  * UserInfo endpoint: `https://services.vib.be/connect/userinfo`
* Next, navigate to `/admin/config/services/vib-service/authorization` and fill
  out settings.
* [OPTIONAL] We advise to only allow administrators to create accounts to make
  sure all "normal" accounts are created through VIB Services

# Configuration VIB Service

Login as an admin in VIB Service and go to "Settings" -> "Apps" -> "Your app"
and edit and configure the URL's.

* POST LOGIN REDIRECT URLS:
  * `https://yourwebsite.com/openid-connect/vib_service`
  * `https://yourwebsite.com/openid-connect/vib_service/silent`
* FRONT CHANNEL LOGOUT URL:
  * `https://yourwebsite.com/openid-connect/channel-logout`

# Important
* After installing this module, the Drupal login form will be accessible on
  `/drupal/login`, the default login page `/user/login` will be redirected to
  the VIB Service authorization page.
* To redirect the user back to a specific page after authorizing,
  just add the query param `open-id-connect-redirect` to `/user/login`.
  This query param is set to the current page and added automatically if the Url
  is rendered through the link render tree.
  See `vib_service_authorization_link_alter()`
* The `/user/logout` page will logout the user from Drupal and from VIB Services
  (if connected).
* The `/user/{user}/edit` page will redirect to VIB Services if the account is
  connected with it.
* If, as admin, you want to edit the Drupal user account you can use the path
  `/user/{user}/edit-drupal`. This path has been added as an operation to the
  dropdown buttons.
