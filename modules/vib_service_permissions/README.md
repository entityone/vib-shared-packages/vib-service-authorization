This module adds extended permission functionality for VIB roles, groups, ...

# Configuration

* Navigate to `admin/config/services/vib-service/permissions`
and enable the entity types you want to restrict access for.
* Clear cache: `drush cr`
* Navigate to one of the entities you enabled previously.
An extra tab "Restrict access" should be available.
* Assign the permission `bypass entity access check` to roles
for which these access restrictions do not apply, eg admin.
