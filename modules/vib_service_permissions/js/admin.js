(function ($) {

  $('.check-uncheck-all').on('click', function () {
    $('input[type="checkbox"]').not($(this)).prop('checked', $(this).prop('checked'));
  })

})(jQuery);
