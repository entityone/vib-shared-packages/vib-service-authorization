<?php

namespace Drupal\vib_service_permissions;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\vib_service_authorization\VibServiceHelper;
use Drupal\vib_service_authorization\VibServiceUserManager;
use Drupal\vib_service_permissions\Entity\EntityAccessInterface;
use Drupal\vib_service_permissions\Plugin\VibService\EntityAccessType\ContentEntityType;
use Drupal\vib_service_permissions\Plugin\VibService\EntityAccessType\InlineBlock;

/**
 * Class EntityAccess.
 *
 * @package Drupal\vib_service_permissions
 */
class EntityAccess {

  /**
   * The entity access helper.
   *
   * @var \Drupal\vib_service_permissions\EntityAccessHelperInterface
   */
  protected $entityAccessHelper;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The vib service helper.
   *
   * @var \Drupal\vib_service_authorization\VibServiceHelper
   */
  protected $vibServiceHelper;

  /**
   * The vib service user manager.
   *
   * @var \Drupal\vib_service_authorization\VibServiceUserManager
   */
  protected $userManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * EntityAccess constructor.
   *
   * @param \Drupal\vib_service_permissions\EntityAccessHelperInterface $entity_access_helper
   *   The entity access helper.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\vib_service_authorization\VibServiceHelper $vib_service_helper
   *   The vib service helper.
   * @param \Drupal\vib_service_authorization\VibServiceUserManager $user_manager
   *   The vib service user manager.
   */
  public function __construct(EntityAccessHelperInterface $entity_access_helper, EntityTypeManagerInterface $entity_type_manager, VibServiceHelper $vib_service_helper, VibServiceUserManager $user_manager) {
    $this->entityAccessHelper = $entity_access_helper;
    $this->entityTypeManager = $entity_type_manager;
    $this->vibServiceHelper = $vib_service_helper;
    $this->userManager = $user_manager;
  }

  /**
   * Returns the access result for the given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check the access for.
   * @param string $operation
   *   The operation.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\vib_service\Client\VibClientException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getResult(EntityInterface $entity, string $operation, AccountInterface $account) {
    $neutral = AccessResult::neutral();
    $neutral->addCacheableDependency($entity);
    $neutral->addCacheContexts(['user']);

    if ($operation !== 'view') {
      // For now, we only support extended access restrictions on view operation.
      return $neutral;
    }

    $entity_access_type = $this->entityAccessHelper->determineEntityAccessType($entity);
    if ($entity_access_type === ContentEntityType::PLUGIN_ID && !$this->entityAccessHelper->entityTypeIsEnabled($entity->getEntityType())) {
      // Entity type is not enabled.
      return $neutral;
    }
    if ($entity_access_type === InlineBlock::PLUGIN_ID && !$this->entityAccessHelper->inlineBlockIsEnabled()) {
      // Inline blocks is not enabled.
      return $neutral;
    }

    if ($account->hasPermission('bypass entity access check')) {
      // Bypass access checks.
      return AccessResult::allowed()
        ->addCacheableDependency($entity)
        ->addCacheContexts(['user']);
    }

    /** @var \Drupal\vib_service_permissions\EntityAccessStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('vib_entity_access');
    if (!$entity_access = $storage->loadForContext(new EntityAccessContext($entity, $entity_access_type))) {
      // Entity has no restrictions.
      return $neutral;
    }

    // Init forbidden.
    $forbidden = AccessResult::forbidden()
      ->addCacheableDependency($entity_access)
      ->addCacheContexts(['user']);

    $allows_anonymous = $entity_access->allowsAnonymousRole();
    if ($account->isAnonymous() && !$allows_anonymous) {
      // Anon has no access.
      return $forbidden;
    }
    if ($account->isAnonymous() && $allows_anonymous) {
      // Anon has access.
      return $neutral;
    }

    // At this point we are checking an authenticated account.
    if (!$this->vibServiceHelper->hasConnectedAccounts($account)) {
      // Account is not connected to VIB services. No access.
      return $forbidden;
    }

    $user_resynced = FALSE;

    // -------------- CHECK TEAMS.
    if (!$vib_user_teams = $this->userManager->getUserVibTeamsForAccount($account)) {
      // Make sure user roles are synced by re-syncing account.
      $this->vibServiceHelper->reSyncAccount($account);
      $user_resynced = TRUE;

      if (!$vib_user_teams = $this->userManager->getUserVibTeamsForAccount($account)) {
        // User has still has no VIB roles, forbidden.
        return $forbidden;
      }
    }

    foreach ($vib_user_teams as $team) {
      if ($entity_access->allowsTeam($team)) {
        // Role has access to entity.
        return $neutral;
      }
    }

    // -------------- CHECK ROLES (DEPRECATED).
    if (!$vib_user_roles = $this->userManager->getUserVibRolesForAccount($account)) {
      if (!$user_resynced) {
        // Make sure user roles are synced by re-syncing account.
        $this->vibServiceHelper->reSyncAccount($account);
      }

      if (!$vib_user_roles = $this->userManager->getUserVibRolesForAccount($account)) {
        // User has still has no VIB roles, forbidden.
        return $forbidden;
      }
    }

    foreach ($vib_user_roles as $role) {
      if ($entity_access->allowsRole($role)) {
        // Role has access to entity.
        return $neutral;
      }
    }

    return $forbidden;
  }

  /**
   * Alters query to take entity access into account.
   *
   * @param \Drupal\Core\Database\Query\SelectInterface $query
   *   The query.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function alterQuery(SelectInterface $query, AccountInterface $account) {
    if (!$entity_type = $query->getMetaData('entity_type')) {
      return;
    }

    if (!empty($query->getMetaData('op')) && $query->getMetaData('op') !== 'view') {
      // For now we only support extended access restrictions on view operation.
      return;
    }

    if ($account->hasPermission('bypass entity access check')) {
      return;
    }

    $key_field = $this->entityTypeManager->getDefinition($entity_type)
      ->getKey('id');

    $tables = $query->getTables();
    $base_table = $query->getMetaData('base_table');

    // If the base table is not given, default to one of the entity base tables.
    if (!$base_table) {
      /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
      $table_mapping = $this->entityTypeManager->getStorage($entity_type)
        ->getTableMapping();
      $base_tables = $table_mapping->getTableNames();

      foreach ($tables as $table_info) {
        if (!($table_info instanceof SelectInterface)) {
          $table = $table_info['table'];
          // Ensure that data tables are always preferred over revision tables.
          if ($table == $table_mapping->getBaseTable() || $table == $table_mapping->getDataTable()) {
            $base_table = $table;
            break;
          }

          // If one of the base tables are in the query, add it to the list
          // of possible base tables to join against.
          if (in_array($table, $base_tables)) {
            $base_table = $table;
          }
        }
      }

      // Bail out if the base table is missing.
      if (!$base_table) {
        throw new \Exception('Query tagged for entity access but there is no base table, specify the base_table using meta data.');
      }
    }

    $vib_roles = array_merge($this->userManager->getUserVibRolesForAccount($account), [EntityAccessInterface::ANONYMOUS_ROLE]);

    // Build sub query to restrict on configured roles.
    $sub_query = $this->connection()->select('vib_entity_access', 'ea');
    $sub_query->fields('ea', ['target_id']);
    $sub_query->innerJoin('vib_entity_access__roles', 'r', 'ea.id = r.entity_id');
    $sub_query->condition('ea.target_type', $entity_type);
    $sub_query->condition('r.roles_value', $vib_roles, 'IN');
    $sub_query->where("$base_table.$key_field = ea.target_id");
    // Or, if no restrictions are configured,
    // to make sure that no restrictions are added.
    $sub_query_two = $this->connection()->select('vib_entity_access', 'ea');
    $sub_query_two->fields('ea', ['target_id']);
    $sub_query_two->condition('ea.target_type', $entity_type);
    $sub_query_two->where("$base_table.$key_field = ea.target_id");

    $or = new Condition('OR');
    $or->exists($sub_query);
    $or->isNull($sub_query_two);

    $query->condition($or);
  }

  /**
   * Returns database connection.
   *
   * @return \Drupal\Core\Database\Connection
   *   The database connection.
   */
  protected function connection() {
    if (!$this->connection) {
      $this->connection = \Drupal::database();
    }

    return $this->connection;
  }

}
