<?php

namespace Drupal\vib_service_permissions;

/**
 * Interface EntityAccessStorageInterface.
 *
 * @package Drupal\vib_service_permissions
 */
interface EntityAccessStorageInterface {

  /**
   * Returns the first matching access entity for a given context.
   *
   * @param \Drupal\vib_service_permissions\EntityAccessContext $context
   *   The context.
   *
   * @return \Drupal\vib_service_permissions\Entity\EntityAccessInterface
   *   A access entity.
   */
  public function loadForContext(EntityAccessContext $context);

  /**
   * Returns a list of access entities for a given context.
   *
   * @param \Drupal\vib_service_permissions\EntityAccessContext $context
   *   The context.
   *
   * @return \Drupal\vib_service_permissions\Entity\EntityAccessInterface[]
   *   A list of entities.
   */
  public function loadForContextMultiple(EntityAccessContext $context);

}
