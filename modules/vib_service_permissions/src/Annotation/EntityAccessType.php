<?php

namespace Drupal\vib_service_permissions\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a EntityAccessType annotation object.
 *
 * @Annotation
 */
class EntityAccessType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
