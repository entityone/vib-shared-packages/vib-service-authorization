<?php

namespace Drupal\vib_service_permissions;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\layout_builder\InlineBlockUsageInterface;
use Drupal\vib_service_permissions\Entity\EntityAccessInterface;
use Drupal\vib_service_permissions\Plugin\VibService\EntityAccessType\ContentEntityType;
use Drupal\vib_service_permissions\Plugin\VibService\EntityAccessType\InlineBlock;

/**
 * Class EntityAccessHelper.
 *
 * @package Drupal\vib_service_permissions
 */
class EntityAccessHelper implements EntityAccessHelperInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The inline block usage manager.
   *
   * @var \Drupal\layout_builder\InlineBlockUsageInterface
   */
  protected $blockUsage;

  /**
   * EntityAccessHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\layout_builder\InlineBlockUsageInterface $block_usage
   *   The inline block usage manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, InlineBlockUsageInterface $block_usage) {
    $this->entityTypeManager = $entity_type_manager;
    $this->config = $config_factory->get('vib_service_permissions.settings');
    $this->blockUsage = $block_usage;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypes($only_enabled = TRUE) {
    return array_filter($this->entityTypeManager->getDefinitions(), function (EntityTypeInterface $entity_type) use ($only_enabled) {
      if (!$this->entityTypeIsApplicable($entity_type)) {
        return FALSE;
      }

      if ($only_enabled && !$this->entityTypeIsEnabled($entity_type)) {
        return FALSE;
      }

      return TRUE;
    });
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeIsEnabled(EntityTypeInterface $entity_type) {
    if (!$this->entityTypeIsApplicable($entity_type)) {
      return FALSE;
    }
    $entity_types = $this->config->get('entity_types') ?: [];
    $enabled = array_keys(array_filter($entity_types));

    return in_array($entity_type->id(), $enabled);
  }

  /**
   * {@inheritdoc}
   */
  public function inlineBlockIsEnabled() {
    $entity_types = $this->config->get('entity_types') ?: [];
    $enabled = array_keys(array_filter($entity_types));

    return in_array(EntityAccessInterface::LAYOUT_BUILDER_INLINE_BLOCK_TYPE, $enabled);
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeIsApplicable(EntityTypeInterface $entity_type) {
    if (!$entity_type instanceof ContentEntityTypeInterface) {
      return FALSE;
    }
    if (!$entity_type->hasLinkTemplate('edit-form') || !$entity_type->getFormClass('edit')) {
      return FALSE;
    }

    return $entity_type->id() !== 'vib_entity_access';
  }

  /**
   * {@inheritdoc}
   */
  public function determineEntityAccessType(EntityInterface $entity) {
    if (!$entity instanceof BlockContentInterface) {
      return ContentEntityType::PLUGIN_ID;
    }

    // Block content can be an inline block used in layout builder.
    return $this->blockUsage->getUsage($entity->id()) ? InlineBlock::PLUGIN_ID : ContentEntityType::PLUGIN_ID;
  }

}
