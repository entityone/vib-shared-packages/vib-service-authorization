<?php

namespace Drupal\vib_service_permissions;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Class EntityAccessStorage.
 *
 * @package Drupal\EntityAccessStorage
 */
class EntityAccessStorage extends SqlContentEntityStorage implements EntityAccessStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadForContext(EntityAccessContext $context) {
    $entities = $this->loadForContextMultiple($context);
    return $entities ? reset($entities) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function loadForContextMultiple(EntityAccessContext $context) {
    $query = $this->getQuery()
      ->accessCheck(FALSE);

    if ($entity = $context->getEntity()) {
      $query->condition('target_type', $entity->getEntityTypeId());
      $query->condition('target_id', $entity->id());
    }
    if ($plugin_id = $context->getType()) {
      $query->condition('plugin', $plugin_id);
    }

    // Load by either teams or roles, not both.
    if ($teams = $context->getTeams()) {
      $query->condition('teams', $teams, 'IN');
    }
    elseif ($roles = $context->getRoles()) {
      $query->condition('roles', $roles, 'IN');
    }

    $result = $query->execute();
    return $result ? $this->loadMultiple($result) : [];
  }

}
