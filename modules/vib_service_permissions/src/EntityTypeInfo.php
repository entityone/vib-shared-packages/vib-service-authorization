<?php

namespace Drupal\vib_service_permissions;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityTypeInfo.
 *
 * @package Drupal\vib_service_permissions
 */
class EntityTypeInfo implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity access helper.
   *
   * @var \Drupal\vib_service_permissions\EntityAccessHelperInterface
   */
  protected $entityAccessHelper;

  /**
   * Constructs a EntityTypeInfo object.
   *
   * @param \Drupal\vib_service_permissions\EntityAccessHelperInterface $entity_access_helper
   *   The entity access helper.
   */
  public function __construct(EntityAccessHelperInterface $entity_access_helper) {
    $this->entityAccessHelper = $entity_access_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('vib_service_permissions.entity_access_helper')
    );
  }

  /**
   * Adds entity access links to appropriate entity types.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The master entity type list to alter.
   *
   * @see hook_entity_type_alter()
   */
  public function entityTypeAlter(array &$entity_types) {
    foreach ($entity_types as $entity_type_id => $entity_type) {
      if (!$this->entityAccessHelper->entityTypeIsEnabled($entity_type)) {
        continue;
      }

      $link = $entity_type->getLinkTemplate('edit-form');
      $entity_types[$entity_type_id]->setLinkTemplate('vib-entity-access-teams', str_replace('edit', 'vib-entity-access-teams', $link));
      $entity_types[$entity_type_id]->setLinkTemplate('vib-entity-access-roles', str_replace('edit', 'vib-entity-access-roles', $link));
    }
  }

  /**
   * Adds entity access operations on entity that supports it.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity on which to define an operation.
   *
   * @return array
   *   An array of operation definitions.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   *
   * @see hook_entity_operation()
   */
  public function entityOperation(EntityInterface $entity) {
    $operations = [];
    if (!$this->entityAccessHelper->entityTypeIsEnabled($entity->getEntityType())) {
      return $operations;
    }

    $operations['vib_entity_access_teams'] = [
      'title' => $this->t('Restrict access (Teams)'),
      'weight' => 99,
      'url' => $entity->toUrl('vib-entity-access-teams'),
    ];

    $operations['vib_entity_access_roles'] = [
      'title' => $this->t('Restrict access (Roles - DEPRECATED)'),
      'weight' => 99,
      'url' => $entity->toUrl('vib-entity-access-roles'),
    ];

    return $operations;
  }

}
