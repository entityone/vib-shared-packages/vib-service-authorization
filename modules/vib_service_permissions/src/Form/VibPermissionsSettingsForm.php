<?php

namespace Drupal\vib_service_permissions\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\vib_service_permissions\Entity\EntityAccessInterface;
use Drupal\vib_service_permissions\EntityAccessHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the permissions settings form.
 */
class VibPermissionsSettingsForm extends ConfigFormBase {

  /**
   * The entity access helper.
   *
   * @var \Drupal\vib_service_permissions\EntityAccessHelperInterface
   */
  protected EntityAccessHelperInterface $entityAccessHelper;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs the permissions form class.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\vib_service_permissions\EntityAccessHelperInterface $entity_access_helper
   *   The entity access helper.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityAccessHelperInterface $entity_access_helper,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($config_factory);
    $this->entityAccessHelper = $entity_access_helper;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('vib_service_permissions.entity_access_helper'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vib_permissions_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vib_service_permissions.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('vib_service_permissions.settings');

    $options = [];
    foreach ($this->entityAccessHelper->getEntityTypes(FALSE) as $entity_type) {
      $options[$entity_type->id()] = $entity_type->getLabel();
    }

    // Add option to enable entity access on layout builder blocks.
    if ($this->moduleHandler->moduleExists('layout_builder')) {
      $options[EntityAccessInterface::LAYOUT_BUILDER_INLINE_BLOCK_TYPE] = $this->t('Layout builder block');
    }

    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled entities'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $settings->get('entity_types'),
      '#description' => $this->t('Select the entity types you want to enable custom VIB permissions for.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('vib_service_permissions.settings');

    $settings->set('entity_types', $form_state->getValue('entity_types'))
      ->save();

    // Must rebuild caches for settings to take immediate effect.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
