<?php

namespace Drupal\vib_service_permissions\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Class EntityAccessDeleteForm.
 *
 * @package Drupal\vib_service_permissions\Form
 */
class EntityAccessDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    if (!$destination = $this->getRequest()->query->get('destination')) {
      throw new \Exception('Empty destination query param');
    }

    return Url::fromUserInput($destination);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDeletionMessage() {
    return $this->t('Access has been reverted to global settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to revert access back to the global settings for %label?', [
      '%label' => $this->getEntity()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return $this->getCancelUrl();
  }

}
