<?php

namespace Drupal\vib_service_permissions\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\vib_service\Client\VibClientException;
use Drupal\vib_service\Client\VibServiceClientInterface;
use Drupal\vib_service_permissions\Entity\EntityAccessInterface;
use Drupal\vib_service_permissions\Plugin\EntityAccessTypePluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityAccessForm.
 *
 * @package Drupal\vib_service_permissions\Form
 */
class EntityAccessForm extends ContentEntityForm {

  /**
   * The VIB http client.
   *
   * @var \Drupal\vib_service\Client\VibServiceClientInterface
   */
  protected $vibClient;

  /**
   * The plugin manager.
   *
   * @var \Drupal\vib_service_permissions\Plugin\EntityAccessTypePluginManager
   */
  protected $pluginManager;

  /**
   * EntityAccessForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\vib_service\Client\VibServiceClientInterface $client
   *   The VIB http client.
   * @param \Drupal\vib_service_permissions\Plugin\EntityAccessTypePluginManager $plugin_manager
   *   The plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, VibServiceClientInterface $client, EntityAccessTypePluginManager $plugin_manager, EntityTypeBundleInfoInterface $entity_type_bundle_info = NULL, TimeInterface $time = NULL) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->vibClient = $client;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('vib_service.client'),
      $container->get('plugin.manager.vib_service_entity_access_type'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    if (!$plugin_id = $route_match->getRouteObject()
      ->getOption('_entity_access_type')) {
      throw new \Exception('Empty _entity_access_type option');
    }

    /** @var \Drupal\vib_service_permissions\Plugin\VibService\EntityAccessTypeInterface $entity_access_type */
    $entity_access_type = $this->pluginManager->createInstance($plugin_id);
    return $entity_access_type->getEntityFromRouteMatch($route_match);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\vib_service_permissions\Entity\EntityAccessInterface $entity */
    $entity = $this->entity;
    $form = parent::buildForm($form, $form_state);

    if ($errors = $entity->getErrors()) {
      // Entity contains errors, show them and shortcut.
      $form['errors'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [
            [
              '#theme' => 'item_list',
              '#items' => $errors,
            ],
          ],
        ],
        '#status_headings' => [
          'status' => t('Status message'),
          'error' => t('Error message'),
          'warning' => t('Warning message'),
        ],
      ];

      return $form;
    }

    $form['#title'] = $this->t('Access restriction matrix for "%title".',
      ['%title' => $entity->label()]);

    $form['header'] = [
      '#type' => 'item',
      [
        '#markup' => $this->t('Below is the access restriction matrix for "%title".',
          ['%title' => $entity->getTargetEntity()->label()]),
      ],
      [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [
            [
              '#theme' => 'item_list',
              '#items' => [
                $this->t('When no specific restrictions are set, this item is visible for every one (= everything is <i>checked</i>).'),
                $this->t('If all teams are <i>unchecked</i>, only editors can view the items.'),
              ],
            ],
          ],
        ],
        '#status_headings' => [
          'status' => t('Status message'),
          'error' => t('Error message'),
          'warning' => t('Warning message'),
        ],
      ],
    ];

    $teams = $entity->getTeams();
    try {
      $vib_teams = $this->vibClient->apiGetTeams();

      $form['all'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Select / deselect all'),
        '#default_value' => $this->entityAccessEverythingChecked($vib_teams),
        '#attributes' => [
          'class' => ['check-uncheck-all'],
        ],
      ];

      $form['teams'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Teams'),
        '#tree' => TRUE,
        '#attributes' => [
          'class' => ['entity-access-teams'],
        ],
      ];

      foreach ($vib_teams as $team) {
        $form['teams'][$team->getFieldKey()] = [
          '#type' => 'item',
          '#title' => $team->getName(),
          'view' => [
            '#type' => 'checkbox',
            '#title' => $this->t('View item'),
            '#default_value' => in_array($team->getFieldKey(), $teams) || $entity->isNew(),
          ],
          'edit' => [
            '#type' => 'checkbox',
            '#title' => $this->t('Edit item'),
            '#access' => FALSE,
          ],
          'delete' => [
            '#type' => 'checkbox',
            '#title' => $this->t('Delete item'),
            '#access' => FALSE,
          ],
        ];
      }
    }
    catch (VibClientException $e) {
      $this->messenger()->addWarning($this->t('Could not fetch VIB teams'));
    }

    $form['#attached']['library'][] = 'vib_service_permissions/admin';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntity(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\vib_service_permissions\Entity\EntityAccessInterface $entity */
    $entity = parent::buildEntity($form, $form_state);

    $teams = array_keys(array_filter($form_state->getValue('teams'), function (array $team) {
      return !empty($team['view']);
    }));

    $entity->setTeams($teams);

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(array $form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\vib_service_permissions\Entity\EntityAccessInterface $entity
     */
    $entity = $this->entity;
    if ($entity->getErrors()) {
      // Entity contains errors, do not allow to save.
      return [];
    }

    $actions = parent::actions($form, $form_state);

    $actions['submit']['#value'] = $this->t('Restrict access');

    if (isset($actions['delete'])) {
      $actions['delete']['#title'] = $this->t('Revert access back to global settings');
      /**
       * @var \Drupal\Core\Url $url
       */
      $url = &$actions['delete']['#url'];
      // Add current url to route object, so we can redirect back after delete.
      $url->setRouteParameter(
        'destination',
        Url::fromUserInput($this->getRequest()->getRequestUri())->toString()
      );
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    parent::save($form, $form_state);

    $this->messenger()
      ->addMessage($this->t('Access restricted to %label.', [
        '%label' => $entity->label(),
      ]));
  }

  /**
   * Checks if everything is checked.
   *
   * @param \Drupal\vib_service\Client\Model\VibTeam[] $vib_teams
   *   The VIB teams.
   *
   * @return bool
   *   Flag indicating if everything is checked.
   */
  protected function entityAccessEverythingChecked(array $vib_teams): bool {
    if ($this->entity->isNew()) {
      // Everything is checked by default.
      return TRUE;
    }

    $teams = $this->entity->getTeams();
    foreach ($vib_teams as $vib_team) {
      if (!in_array($vib_team->getFieldKey(), $teams)) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
