<?php

namespace Drupal\vib_service_permissions;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Class EntityAccessAccessControlHandler.
 *
 * @package Drupal\vib_service_permissions
 */
class EntityAccessAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\vib_service_permissions\Entity\EntityAccessInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished entity access entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published entity access entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit entity access entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete entity access entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add entity access entities');
  }

}
