<?php

namespace Drupal\vib_service_permissions;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Interface EntityAccessHelperInterface.
 *
 * @package Drupal\vib_service_permissions
 */
interface EntityAccessHelperInterface {

  /**
   * Returns a list of entity types that can be used for the entity access.
   *
   * @param bool $only_enabled
   *   Only load enabled entity types.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   A list of entity types that can be referenced.
   */
  public function getEntityTypes($only_enabled = TRUE);

  /**
   * Returns a flag indicating if entity type is enabled for entity access.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   Flag indicating if entity type is enabled for entity access.
   */
  public function entityTypeIsEnabled(EntityTypeInterface $entity_type);

  /**
   * Returns a flag indicating if inline block is enabled for entity access.
   *
   * @return bool
   *   Flag indicating if inline block is enabled for entity access.
   */
  public function inlineBlockIsEnabled();

  /**
   * Returns a flag indicating if entity type is applicable for entity access.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return bool
   *   Flag indicating if entity type is applicable for entity access.
   */
  public function entityTypeIsApplicable(EntityTypeInterface $entity_type);

  /**
   * Determines the entity access type by a given entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return string
   *   The entity access type.
   */
  public function determineEntityAccessType(EntityInterface $entity);

}
