<?php

namespace Drupal\vib_service_permissions;

use Drupal\Core\Entity\EntityInterface;

/**
 * Class EntityAccessContext.
 *
 * @package Drupal\vib_service_permissions
 */
class EntityAccessContext {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The plugin ID.
   *
   * @var string
   */
  protected $type;

  /**
   * The roles.
   *
   * @var array
   */
  protected $roles;

  /**
   * The teams.
   *
   * @var array
   */
  protected $teams;

  /**
   * EntityAccessContext constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity.
   * @param string|null $type
   *   The plugin ID.
   * @param array|null $roles
   *   A list of VIB roles.
   * @param array|null $teams
   *   A list of VIB teams.
   */
  public function __construct(EntityInterface $entity = NULL, string $type = NULL, array $roles = NULL, array $teams = NULL) {
    $this->entity = $entity;
    $this->type = $type;
    $this->roles = $roles;
    $this->teams = $teams;
  }

  /**
   * Returns the entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Returns the plugin ID.
   *
   * @return string|null
   *   The plugin ID.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Returns a list of VIB roles.
   *
   * @return array|null
   *   A list of VIB roles.
   */
  public function getRoles() {
    return $this->roles;
  }

  /**
   * Returns a list of VIB teams.
   *
   * @return array|null
   *   A list of VIB teams.
   */
  public function getTeams() {
    return $this->teams;
  }

}
