<?php

namespace Drupal\vib_service_permissions\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Interface EntityAccessInterface.
 *
 * @package Drupal\vib_service_permissions\Entity
 */
interface EntityAccessInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  const OPERATION_VIEW = 'view';
  const OPERATION_UPDATE = 'update';
  const OPERATION_DELETE = 'delete';
  const OPERATION_ALL = [
    self::OPERATION_VIEW => 'View',
    self::OPERATION_UPDATE => 'Edit',
    self::OPERATION_DELETE => 'Delete',
  ];

  const ANONYMOUS_ROLE = 'anonymous';
  const LAYOUT_BUILDER_INLINE_BLOCK_TYPE = 'layout_builder_inline_block';

  /**
   * Returns the entity for which the access applies to.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The entity to restrict.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTargetEntity();

  /**
   * Returns the target entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getTargetType();

  /**
   * Returns the target entity id.
   *
   * @return int
   *   The entity id.
   */
  public function getTargetId();

  /**
   * Returns the type plugin.
   *
   * @return \Drupal\vib_service_permissions\Plugin\VibService\EntityAccessTypeInterface
   *   The type plugin.
   */
  public function getPlugin();

  /**
   * Returns a list of roles.
   *
   * @return string[]
   *   A list of roles.
   *
   * @deprecated The roles will not be checked for long, use getTeams instead.
   */
  public function getRoles();

  /**
   * Sets the roles.
   *
   * @param array $role_ids
   *   A list of roles ids.
   *
   * @return \Drupal\vib_service_permissions\Entity\EntityAccessInterface
   *   The entity.
   *
   * @deprecated The roles will not be checked for long, use setTeams instead.
   */
  public function setRoles(array $role_ids);

  /**
   * Checks if role is allowed.
   *
   * @param string $role
   *   The role.
   *
   * @return bool
   *   Flag indicating if role is allowed.
   *
   * @deprecated The roles will not be checked for long, use allowsTeam instead.
   */
  public function allowsRole(string $role);

  /**
   * Checks if anon role is allowed.
   *
   * @return bool
   *   Flag indicating if anon role is allowed.
   */
  public function allowsAnonymousRole();

  /**
   * Returns a list of teams.
   *
   * @return string[]
   *   A list of teams.
   */
  public function getTeams();

  /**
   * Sets the teams.
   *
   * @param array $team_ids
   *   A list of team ids.
   *
   * @return \Drupal\vib_service_permissions\Entity\EntityAccessInterface
   *   The entity.
   */
  public function setTeams(array $team_ids);

  /**
   * Checks if team is allowed.
   *
   * @param string $team
   *   The team.
   *
   * @return bool
   *   Flag indicating if team is allowed.
   */
  public function allowsTeam(string $team);

  /**
   * Returns a list of errors at runtime.
   *
   * @return array
   *   A list of errors.
   */
  public function getErrors();

  /**
   * Adds an error message to the list of errors.
   *
   * @param string $message
   *   The error message.
   *
   * @return \Drupal\vib_service_permissions\Entity\EntityAccessInterface
   *   The called Entity access entity.
   */
  public function addError($message);

  /**
   * Gets the Entity access creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Entity access.
   */
  public function getCreatedTime();

  /**
   * Sets the Entity access creation timestamp.
   *
   * @param int $timestamp
   *   The Entity access creation timestamp.
   *
   * @return \Drupal\vib_service_permissions\Entity\EntityAccessInterface
   *   The called Entity access entity.
   */
  public function setCreatedTime($timestamp);

}
