<?php

namespace Drupal\vib_service_permissions\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Entity access entities.
 */
class EntityAccessViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
