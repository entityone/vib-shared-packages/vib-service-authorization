<?php

namespace Drupal\vib_service_permissions\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\user\UserInterface;
use Drupal\vib_service\Client\VibClientException;

/**
 * Defines the Entity access entity.
 *
 * @ingroup vib_service_permissions
 *
 * @ContentEntityType(
 *   id = "vib_entity_access",
 *   label = @Translation("Entity access"),
 *   handlers = {
 *     "views_data" = "Drupal\vib_service_permissions\Entity\EntityAccessViewsData",
 *     "storage" = "Drupal\vib_service_permissions\EntityAccessStorage",
 *     "form" = {
 *       "teams" = "Drupal\vib_service_permissions\Form\EntityAccessForm",
 *       "roles" = "Drupal\vib_service_permissions\Form\EntityAccessRolesForm",
 *       "delete" = "Drupal\vib_service_permissions\Form\EntityAccessDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\vib_service_permissions\EntityAccessHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\vib_service_permissions\EntityAccessAccessControlHandler",
 *   },
 *   base_table = "vib_entity_access",
 *   translatable = FALSE,
 *   admin_permission = "administer entity access entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "delete-form" = "/admin/entity-access/{vib_entity_access}/delete"
 *   },
 *   field_ui_base_route = "vib_entity_access.settings",
 *   constraints = {
 *     "ValidTargetEntity" = {}
 *   }
 * )
 */
class EntityAccess extends ContentEntityBase implements EntityAccessInterface {
  use EntityChangedTrait;

  /**
   * A list of errors added at runtime.
   *
   * @var array
   */
  protected $errors;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->getTargetEntity()->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetEntity() {
    $storage = \Drupal::entityTypeManager()
      ->getStorage($this->getTargetType());
    return $storage->load($this->getTargetId());
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetType() {
    return $this->get('target_type')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetId() {
    return $this->get('target_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    /** @var \Drupal\vib_service_permissions\Plugin\EntityAccessTypePluginManager $manager */
    $manager = \Drupal::service('plugin.manager.vib_service_entity_access_type');
    return $manager->createInstance($this->get('plugin')->value, ['entity' => $this]);
  }

  /**
   * {@inheritdoc}
   */
  public function getRoles() {
    return array_map(function (array $role) {
      return $role['value'];
    }, $this->get('roles')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function setRoles(array $role_ids) {
    $this->set('roles', $role_ids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function allowsRole(string $role) {
    return in_array($role, $this->getRoles());
  }

  /**
   * {@inheritdoc}
   */
  public function allowsAnonymousRole() {
    return $this->allowsRole(EntityAccessInterface::ANONYMOUS_ROLE);
  }

  /**
   * {@inheritdoc}
   */
  public function getTeams() {
    return array_map(function (array $team) {
      return $team['value'];
    }, $this->get('teams')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public function setTeams(array $team_ids) {
    $this->set('teams', $team_ids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function allowsTeam(string $team) {
    return in_array($team, $this->getTeams());
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    if (!$entity = $this->getTargetEntity()) {
      parent::getCacheTagsToInvalidate();
    }
    // Invalidate cache of target entity every time entity access is saved.
    return Cache::mergeTags(
      parent::getCacheTagsToInvalidate(),
      $entity->getCacheTags()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getErrors() {
    return $this->errors;
  }

  /**
   * {@inheritdoc}
   */
  public function addError($message) {
    $this->errors[] = $message;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    /**
     * @var \Drupal\vib_service\Client\VibServiceClientInterface $client
     */
    $client = \Drupal::service('vib_service.client');
    $options = [EntityAccessInterface::ANONYMOUS_ROLE => 'Anonymous'];

    try {
      foreach ($client->apiGetRoles() as $role) {
        $options[$role->getFieldKey()] = $role->getName();
      }
    }
    catch (VibClientException $e) {}

    $fields['roles'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Roles'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', $options)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $options = [];
    try {
      foreach ($client->apiGetTeams() as $team) {
        $options[$team->getFieldKey()] = $team->getName();
      }
    }
    catch (VibClientException $e) {}

    $fields['teams'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Teams'))
      ->setRequired(TRUE)
      ->setSetting('allowed_values', $options)
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $options = [];
    /** @var \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type */
    foreach (\Drupal::service('vib_service_permissions.entity_access_helper')
      ->getEntityTypes() as $entity_type) {
      $options[$entity_type->id()] = $entity_type->getLabel();
    }
    $fields['target_type'] = BaseFieldDefinition::create('list_string')
      ->setLabel('Target type')
      ->setRequired(TRUE)
      ->setSetting('allowed_values', $options)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['target_id'] = BaseFieldDefinition::create('integer')
      ->setLabel('Target ID')
      ->setRequired(TRUE)
      ->setSetting('unsigned', TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Entity access entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['plugin'] = BaseFieldDefinition::create('string')
      ->setLabel('Type')
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
