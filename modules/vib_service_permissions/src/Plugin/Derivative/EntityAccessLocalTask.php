<?php

namespace Drupal\vib_service_permissions\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\vib_service_permissions\EntityAccessHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityAccessLocalTask.
 *
 * @package Drupal\vib_service_permissions\Plugin\Derivative
 */
class EntityAccessLocalTask extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity access helper.
   *
   * @var \Drupal\vib_service_permissions\EntityAccessHelperInterface
   */
  protected $entityAccessHelper;

  /**
   * Constructs a new EntityAccessLocalTask.
   *
   * @param \Drupal\vib_service_permissions\EntityAccessHelperInterface $entity_access_helper
   *   The entity access helper.
   */
  public function __construct(EntityAccessHelperInterface $entity_access_helper) {
    $this->entityAccessHelper = $entity_access_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('vib_service_permissions.entity_access_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    foreach ($this->entityAccessHelper->getEntityTypes() as $entity_type_id => $entity_type) {
      // Add the new teams tab.
      if ($entity_type->hasLinkTemplate('vib-entity-access-teams')) {
        $this->derivatives["$entity_type_id.vib_entity_access_teams"] = [
          'route_name' => "entity.$entity_type_id.vib_entity_access_teams",
          'title' => $this->t('Restrict access (teams)'),
          'base_route' => "entity.$entity_type_id." . ($entity_type->hasLinkTemplate('canonical') ? 'canonical' : 'edit_form'),
          'weight' => 99,
        ];
      }

      // Add the old roles tab.
      if ($entity_type->hasLinkTemplate('vib-entity-access-roles')) {
        $this->derivatives["$entity_type_id.vib_entity_access_roles"] = [
          'route_name' => "entity.$entity_type_id.vib_entity_access_roles",
          'title' => $this->t('Restrict access (roles)'),
          'base_route' => "entity.$entity_type_id." . ($entity_type->hasLinkTemplate('canonical') ? 'canonical' : 'edit_form'),
          'weight' => 99,
        ];
      }
    }

    if ($this->entityAccessHelper->inlineBlockIsEnabled()) {
      // Add local tasks for layout builder inline blocks.
      $this->derivatives['layout_builder.update_block'] = [
        'route_name' => 'layout_builder.update_block',
        'title' => $this->t('Edit'),
        'base_route' => 'layout_builder.update_block',
        'weight' => 90,
      ];

      $this->derivatives['vib_service_permissions.inline_block.vib_entity_access'] = [
        'route_name' => 'vib_service_permissions.inline_block.vib_entity_access',
        'title' => $this->t('Restrict access'),
        'base_route' => 'layout_builder.update_block',
        'weight' => 99,
      ];
    }

    foreach ($this->derivatives as &$entry) {
      $entry += $base_plugin_definition;
    }

    return $this->derivatives;
  }

}
