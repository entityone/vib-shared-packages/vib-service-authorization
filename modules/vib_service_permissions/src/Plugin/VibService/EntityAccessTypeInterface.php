<?php

namespace Drupal\vib_service_permissions\Plugin\VibService;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Interface EntityAccessTypeInterface.
 *
 * @package Drupal\vib_service_permissions\Plugin\VibService
 */
interface EntityAccessTypeInterface extends PluginInspectionInterface {

  /**
   * Returns access entity based on current route.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route.
   *
   * @return \Drupal\vib_service_permissions\Entity\EntityAccessInterface
   *   The entity access entity.
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match);

}
