<?php

namespace Drupal\vib_service_permissions\Plugin\VibService\EntityAccessType;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\vib_service_permissions\Entity\EntityAccess;
use Drupal\vib_service_permissions\EntityAccessContext;
use Drupal\vib_service_permissions\Plugin\VibService\EntityAccessTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity access type plugin for 'content_entity_type'.
 *
 * @EntityAccessType(
 *   id = "content_entity_type",
 *   label = @Translation("Content entity type"),
 * )
 */
class ContentEntityType extends PluginBase implements EntityAccessTypeInterface, ContainerFactoryPluginInterface {

  const PLUGIN_ID = 'content_entity_type';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access entity.
   *
   * @var \Drupal\vib_service_permissions\Entity\EntityAccessInterface
   */
  protected $entity;

  /**
   * Constructs a ContentEntityType object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->entity = $configuration['entity'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match) {
    $entity_type_id = $route_match->getRouteObject()
      ->getOption('_entity_access_entity_type_id');

    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    if (!$entity = $route_match->getParameter($entity_type_id)) {
      throw new \Exception('Invalid _entity_access_entity_type_id option');
    }

    /** @var \Drupal\vib_service_permissions\EntityAccessStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('vib_entity_access');
    if (!$entity_access = $storage->loadForContext(new EntityAccessContext($entity, $this->getPluginId()))) {
      $entity_access = EntityAccess::create([
        'plugin' => $this->getPluginId(),
        'target_type' => $entity->getEntityTypeId(),
        'target_id' => $entity->id(),
      ]);
    }

    return $entity_access;
  }

}
