<?php

namespace Drupal\vib_service_permissions\Plugin\VibService\EntityAccessType;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\vib_service_permissions\Entity\EntityAccess;
use Drupal\vib_service_permissions\EntityAccessContext;
use Drupal\vib_service_permissions\Plugin\VibService\EntityAccessTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity access type plugin for 'inline_block'.
 *
 * @EntityAccessType(
 *   id = "inline_block",
 *   label = @Translation("Inline block"),
 * )
 */
class InlineBlock extends PluginBase implements EntityAccessTypeInterface, ContainerFactoryPluginInterface {

  const PLUGIN_ID = 'inline_block';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The access entity.
   *
   * @var \Drupal\vib_service_permissions\Entity\EntityAccessInterface
   */
  protected $entity;

  /**
   * Constructs a InlineBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->entity = $configuration['entity'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match) {
    /** @var \Drupal\layout_builder\SectionStorageInterface $section_storage */
    if (!$section_storage = $route_match->getParameter('section_storage')) {
      throw new \Exception('Invalid section_storage option');
    }
    if ($route_match->getParameter('delta') === FALSE) {
      throw new \Exception('Invalid delta option');
    }
    $delta = $route_match->getParameter('delta');
    if (!$uuid = $route_match->getParameter('uuid')) {
      throw new \Exception('Invalid uuid option');
    }

    /** @var \Drupal\layout_builder\Plugin\Block\InlineBlock $inline_block */
    if (!$inline_block = $section_storage->getSection($delta)
      ->getComponent($uuid)
      ->getPlugin()) {
      throw new \Exception('Invalid inline block');
    }

    if (empty($inline_block->getConfiguration()['block_revision_id'])) {
      // Block rid does not exist,
      // Because the layout builder content has not been saved yet.
      $entity_access = EntityAccess::create([
        'plugin' => $this->getPluginId(),
      ]);

      // Log error on the object.
      // While rendering the form, we'll show this.
      $entity_access->addError($this->t('You have unsaved changes. Please <a href=":url">save</a> the content before setting restrictions', [
        ':url' => Url::fromRoute('layout_builder.overrides.node.view', [
          'entity_type_id' => 'node',
          'section_storage_type' => $section_storage->getStorageType(),
          'section_storage' => $section_storage,
          'node' => str_replace('node.', '', $section_storage->getStorageId()),
        ],
          [
            'fragment' => 'block-local-tasks-block',
          ])->toString(),
      ]));

      return $entity_access;
    }

    $entity = $this->entityTypeManager->getStorage('block_content')
      ->loadRevision($inline_block->getConfiguration()['block_revision_id']);

    /** @var \Drupal\vib_service_permissions\EntityAccessStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('vib_entity_access');
    if (!$entity_access = $storage->loadForContext(new EntityAccessContext($entity, $this->getPluginId()))) {
      $entity_access = EntityAccess::create([
        'plugin' => $this->getPluginId(),
        'target_type' => $entity->getEntityTypeId(),
        'target_id' => $entity->id(),
      ]);
    }

    return $entity_access;
  }

}
