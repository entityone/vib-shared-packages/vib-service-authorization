<?php

namespace Drupal\vib_service_permissions\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\vib_service_permissions\Entity\EntityAccessInterface;
use Drupal\vib_service_permissions\EntityAccessContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class ValidTargetEntityConstraintValidator.
 *
 * @package Drupal\vib_service_permissions\Plugin\Validation\Constraint
 */
class ValidTargetEntityConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ValidTargetEntityConstraintValidator constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    if (!$entity instanceof EntityAccessInterface) {
      // Shortcut.
      return;
    }
    if (!$entity->isNew()) {
      // Shortcut.
      return;
    }
    if (!$target_entity = $this->entityTypeManager
      ->getStorage($entity->getTargetType())
      ->load($entity->getTargetId())) {
      // Shortcut.
      return;
    }

    /** @var \Drupal\vib_service_permissions\EntityAccessStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('vib_entity_access');
    if ($storage->loadForContext(new EntityAccessContext($target_entity, $entity->getPlugin()->getPluginId()))) {
      $this->context->buildViolation($constraint->notUnique)
        ->atPath('title')
        ->addViolation();

      return;
    }
  }

}
