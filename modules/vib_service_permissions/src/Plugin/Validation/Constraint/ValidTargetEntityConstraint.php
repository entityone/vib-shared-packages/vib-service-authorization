<?php

namespace Drupal\vib_service_permissions\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * The constraint for plugin 'ValidTargetEntity'.
 *
 * @Constraint(
 *   id = "ValidTargetEntity",
 *   label = @Translation("Valid target entity", context = "Validation"),
 *   type = "entity:vib_entity_access"
 * )
 */
class ValidTargetEntityConstraint extends CompositeConstraintBase {

  /**
   * The message template.
   *
   * @var string
   */
  public $notUnique = 'There is an access entity for this target already.';

  /**
   * {@inheritdoc}
   */
  public function coversFields() {
    return ['target_type', 'target_id'];
  }

}
