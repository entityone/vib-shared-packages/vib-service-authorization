<?php

namespace Drupal\vib_service_permissions\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class EntityAccessTypePluginManager.
 *
 * @package Drupal\vib_service_authorization\Plugin\VibService
 */
class EntityAccessTypePluginManager extends DefaultPluginManager {

  /**
   * Creates the EntityAccessTypePluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/VibService/EntityAccessType', $namespaces, $module_handler,
      'Drupal\vib_service_permissions\Plugin\VibService\EntityAccessTypeInterface', 'Drupal\vib_service_permissions\Annotation\EntityAccessType');
  }

}
