<?php

namespace Drupal\vib_service_permissions;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\vib_service_permissions\Plugin\VibService\EntityAccessType\ContentEntityType;
use Drupal\vib_service_permissions\Plugin\VibService\EntityAccessType\InlineBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;

/**
 * Class EntityAccessHtmlRouteProvider.
 *
 * @package Drupal\vib_service_permissions
 */
class EntityAccessHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * The entity access helper.
   *
   * @var \Drupal\vib_service_permissions\EntityAccessHelperInterface
   */
  protected $entityAccessHelper;

  /**
   * Constructs a new EntityAccessHtmlRouteProvider.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\vib_service_permissions\EntityAccessHelperInterface $entity_access_helper
   *   The entity access helper.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager, EntityAccessHelperInterface $entity_access_helper) {
    parent::__construct($entity_type_manager, $entity_field_manager);
    $this->entityAccessHelper = $entity_access_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('vib_service_permissions.entity_access_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);

    $entity_type_id = $entity_type->id();

    if ($settings_form_route = $this->getSettingsFormRoute($entity_type)) {
      $collection->add("$entity_type_id.settings", $settings_form_route);
    }

    foreach ($this->entityAccessHelper->getEntityTypes() as $entity_type_id => $entity_type) {
      if ($route = $this->getEntityAccessTeamsRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.vib_entity_access_teams", $route);
      }
      if ($route = $this->getEntityAccessRolesRoute($entity_type)) {
        $collection->add("entity.$entity_type_id.vib_entity_access_roles", $route);
      }
    }

    if ($this->entityAccessHelper->inlineBlockIsEnabled() && ($route = $this->getInlineBlockAccessRoute($entity_type))) {
      // Provide route for layout builder inline blocks.
      $collection->add("vib_service_permissions.inline_block.vib_entity_access", $route);
    }

    return $collection;
  }

  /**
   * Gets the entity access roles route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|void
   *   The generated route, if available.
   */
  protected function getEntityAccessRolesRoute(EntityTypeInterface $entity_type) {
    if ($link = $entity_type->getLinkTemplate('vib-entity-access-roles')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($link);
      $route
        ->addDefaults([
          '_entity_form' => "vib_entity_access.roles",
          'entity_type_id' => 'vib_entity_access',
        ])
        ->addRequirements([
          '_permission' => 'manage entity access entities',
        ])
        ->setOption('_entity_access_entity_type_id', $entity_type_id)
        ->setOption('_entity_access_type', ContentEntityType::PLUGIN_ID)
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      return $route;
    }
  }

  /**
   * Gets the entity access teams route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|void
   *   The generated route, if available.
   */
  protected function getEntityAccessTeamsRoute(EntityTypeInterface $entity_type) {
    if ($link = $entity_type->getLinkTemplate('vib-entity-access-teams')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($link);
      $route
        ->addDefaults([
          '_entity_form' => "vib_entity_access.teams",
          'entity_type_id' => 'vib_entity_access',
        ])
        ->addRequirements([
          '_permission' => 'manage entity access entities',
        ])
        ->setOption('_entity_access_entity_type_id', $entity_type_id)
        ->setOption('_entity_access_type', ContentEntityType::PLUGIN_ID)
        ->setOption('_admin_route', TRUE)
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      return $route;
    }
  }

  /**
   * Gets the inline block access route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getInlineBlockAccessRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();
    $route = new Route('/layout_builder/update/block/{section_storage_type}/{section_storage}/{delta}/{region}/{uuid}/vib-entity-access-teams');
    $route
      ->addDefaults([
        '_entity_form' => "vib_entity_access.teams",
        'entity_type_id' => 'vib_entity_access',
      ])
      ->addRequirements([
        '_permission' => 'manage entity access entities',
      ])
      ->setOption('_entity_access_type', InlineBlock::PLUGIN_ID)
      ->setOption('_admin_route', TRUE)
      ->setOption('parameters', [
        $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        'section_storage' => ['layout_builder_tempstore' => TRUE],
      ]);

    return $route;
  }

  /**
   * Gets the settings form route.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The generated route, if available.
   */
  protected function getSettingsFormRoute(EntityTypeInterface $entity_type) {
    if (!$entity_type->getBundleEntityType()) {
      $route = new Route("/admin/structure/{$entity_type->id()}/settings");
      $route
        ->setDefaults([
          '_form' => 'Drupal\vib_service_permissions\Form\EntityAccessSettingsForm',
          '_title' => "{$entity_type->getLabel()} settings",
        ])
        ->setRequirement('_permission', $entity_type->getAdminPermission())
        ->setOption('_admin_route', TRUE);

      return $route;
    }
  }

}
